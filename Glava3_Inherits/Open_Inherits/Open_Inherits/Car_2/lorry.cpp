#include "lorry.h"
#include <iostream>

Lorry::Lorry( const std::string & name, int16_t cyl, int16_t pow, int16_t mass )
    : Car( name, cyl, pow )
    , mass_ { mass }
{
}

std::string Lorry::ToString( )
{
    return Car::ToString( ) + ", mass = " + std::to_string( mass_ );
}

void Lorry::Display( )
{
    std::cout << ToString( ) << std::endl;
}

int16_t Lorry::mass( ) const
{
    return mass_;
}

void Lorry::setMass( const int16_t & mass )
{
    mass_ = mass;
}
