#include <iostream>

using namespace std;

class Data10 {
  struct Pair {
    double first;
    double second;
  };

 public:
  void Init( double a, int32_t b );
  void Read( );
  void Display( ) const;
  double Function( double x ) const;

 private:
  Pair data_;
};

int main( ) {
  Data10 dt;
  dt.Init( 3, 4 );
  std::cout << dt.Function( 2 );
}

void Data10::Init( double a, int32_t b ) {
  data_.first = a;
  data_.second = b;
}

void Data10::Read( ) {
  double a, b;
  std::cout << "enter a, b" << std::endl;
  std::cin >> a >> b;
  this->Init( a, b );
}

void Data10::Display( ) const {
  std::cout << "Display "
            << "f: " << data_.first << " :: s: " << data_.second;
}

double Data10::Function( double x ) const {
  return data_.first * x + data_.second;
}
