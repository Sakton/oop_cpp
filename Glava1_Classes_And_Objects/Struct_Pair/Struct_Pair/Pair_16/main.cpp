#include <iomanip>
#include <iostream>

class Data16 {
  struct Pair {
    double first;
    double second;
  };

 public:
  void Init( double cel, double dr );
  void Read( );
  void Display( double num ) const;
  double Multiply( double num ) const;

 private:
  Pair data_;
};

Data16 make_data16( double cel, double dr );

int main()
{
  Data16 d;
  d.Init( 10000, 0.568 );
  d.Display( 100 );
}

void Data16::Init( double cel, double dr ) {
  if ( dr < 0 ) {
    std::cerr << "ERROR";
    exit( 1 );
  }
  data_.first = cel;
  data_.second = dr;
}

void Data16::Read( ) {
  double a, b;
  std::cout << "enter a b" << std::endl;
  std::cin >> a >> b;
  this->Init( a, b );
}

void Data16::Display( double num ) const {
  std::cout << std::fixed << std::setprecision( 0 ) << data_.first << ","
            << static_cast< int >( data_.second * 1000 )
            << "  multip = " << Multiply( num ) << std::endl;
}

double Data16::Multiply( double num ) const {
  return ( data_.first + data_.second ) * num;
}

Data16 make_data16( double cel, double dr ) {
  Data16 loc;
  loc.Init( cel, dr );
  return loc;
}
