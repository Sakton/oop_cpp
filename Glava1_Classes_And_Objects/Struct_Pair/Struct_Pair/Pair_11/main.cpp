#include <iostream>

class Data11 {
    struct Pair {
        double first;
        double second;
    };

public:
    void Init(double a, int32_t b);
    void Read( );
    void Display( ) const;
    double Root( ) const;

   private:
    Pair data_;
};

int main() {
    Data11 dt;
    dt.Init(3, 4);
    std::cout << dt.Root( );
}

void Data11::Init(double a, int32_t b) {
    data_.first = a;
    data_.second = b;
}

void Data11::Read( ) {
  double a, b;
  std::cout << "enter a, b" << std::endl;
  std::cin >> a >> b;
  this->Init( a, b );
}

void Data11::Display( ) const {
  std::cout << "Display "
            << "f: " << data_.first << " :: s: " << data_.second;
}

double Data11::Root( ) const {
  if ( !data_.second ) {
    std::cout << "ERROR";
    exit( 1 );
  }
  return -data_.first / data_.second;
}
