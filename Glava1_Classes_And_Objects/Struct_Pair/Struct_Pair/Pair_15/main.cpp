#include <iostream>

class Data15 {
  struct Pair {
    int first;
    double second;
  };

 public:
  void Init( int min, double price );
  void Read( );
  void Display( ) const;
  double Cost( ) const;

 private:
  Pair data_;
};

#pragma pack( push, 1 )
struct Pair {
  int first;
  double second;
};
#pragma pack( pop )

int main()
{
  std::cout << "pragma = " << sizeof( Pair )
            << " no pragma = " << sizeof( Data15 ) << std::endl;

  Data15 d;
  d.Init( 37, 25.25 );
  d.Display( );
}

void Data15::Init( int min, double price ) {
  if ( min < 0 || min > 59 || price < 0 ) {
    std::cout << "ERROR";
    exit( 1 );
  }
  data_.first = min;
  data_.second = price;
}

void Data15::Read( ) {
  int m = 0;
  double pr = 0;
  std::cout << "enter min price" << std::endl;
  std::cin >> m >> pr;
  Init( m, pr );
}

void Data15::Display( ) const {
  std::cout << data_.first << ", " << data_.second << " cost = " << Cost( )
            << std::endl;
}

double Data15::Cost( ) const { return data_.first * data_.second; }
