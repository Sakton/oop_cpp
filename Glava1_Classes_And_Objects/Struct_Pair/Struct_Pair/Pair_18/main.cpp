#include <iostream>

class Data18 {
 public:
  struct Pair {
    int32_t first;
    uint32_t second;
  };

  void Init( int32_t c, int32_t dr );
  void Read( );
  void Display( ) const;
  Pair Multiplay( int num ) const;

 private:
  Pair data_;
};

int main()
{
  Data18 d;
  d.Init( 12334556, 42342342 );
  Data18::Pair p = d.Multiplay( 1000 );
  std::cout << p.first << "," << p.second;
}

void Data18::Init( int32_t c, int32_t dr ) {
  if ( dr < 0 ) {
    std::cerr << "ERROR";
    exit( 1 );
  }
  data_.first = c;
  data_.second = dr;
}

void Data18::Read( ) {
  int32_t a, b;
  std::cout << "enter a, b" << std::endl;
  std::cin >> a >> b;
  this->Init( a, b );
}

void Data18::Display( ) const {
  std::cout << data_.first << "," << data_.second << std::endl;
}

Data18::Pair Data18::Multiplay( int num ) const {
  int64_t cel = data_.first * num;
  int64_t drobn = data_.second * num;
  if ( drobn > std::numeric_limits< int32_t >::max( ) ) {
    cel += drobn / std::numeric_limits< int32_t >::max( );
    drobn = drobn % std::numeric_limits< int32_t >::max( );
  }
  if ( cel > std::numeric_limits< int32_t >::max( ) || cel < 0 ) {
    std::cerr << "OVERFLOW";
    exit( 1 );
  }
  Pair res;
  res.first = cel;
  res.second = drobn;
  return res;
}
