#include <iostream>

class Data12 {
    struct Pair {
        double first;
        double second;
    };

public:
    void Init(double x = 0, double y = 0);
    void Read();
    void Display() const;
    double Distance() const;

private:
    Pair data_;
};

Data12 make_Data12(double x, double y);

#pragma pack(push, 1)
struct Pair {
    double first;
    double second;
};
#pragma pack(pop)

int main() {
    Data12 dt;
    dt.Init(3, 4);
    dt.Display();
}

void Data12::Init(double x, double y) {
    data_.first = x;
    data_.second = y;
}

void Data12::Read() {
    std::cout << "enter x, y" << std::endl;
    //тут можно
    std::cin >> data_.first >> data_.second;
}

void Data12::Display() const {
    std::cout << data_.first << ":" << data_.second << " l = " << Distance()
              << std::endl;
}

double Data12::Distance() const {
    return std::sqrt(std::pow(data_.first, 2) + std::pow(data_.second, 2));
}

Data12 make_Data12(double x, double y) {
    Data12 loc;
    loc.Init(x, y);
    return loc;
}
