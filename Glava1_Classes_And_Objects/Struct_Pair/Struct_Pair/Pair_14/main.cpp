#include <iostream>

class Data14 {
  struct Pair {
    double first;
    int second;
  };

 public:
  void Init( double okl, int days );
  void Read( );
  void Display( int dayInmon ) const;
  double Cost( int dayInMonat ) const;

 private:
  Pair data_;
};

Data14 make_data14( double okl, int days );

#pragma pack( push, 1 )
struct Pair {
  double first;
  int second;
};
#pragma pack( pop )

int main( ) {
  std::cout << "pragma = " << sizeof( Pair )
            << " no pragma = " << sizeof( Data14 ) << std::endl;
  Data14 d;
  d.Init( 100000, 24 );
  d.Display( 31 );

  Data14 k = make_data14( 200000, 20 );
  k.Display( 31 );
}

void Data14::Init( double okl, int days ) {
  if ( days < 0 || days > 31 || okl < 0 ) {
    std::cout << "ERROR";
    exit( 1 );
  }
  data_.first = okl;
  data_.second = days;
}

void Data14::Read( ) {
  double ok = 0;
  int d = 0;
  std::cout << "enter okl day" << std::endl;
  std::cin >> ok >> d;
  this->Init( ok, d );
}

void Data14::Display( int dayInmon ) const {
  std::cout << data_.first << ", " << data_.second
            << " Cost = " << Cost( dayInmon ) << std::endl;
}

double Data14::Cost( int dayInMonat ) const {
  return ( data_.first / dayInMonat ) * data_.second;
}

Data14 make_data14( double okl, int days ) {
  Data14 loc;
  loc.Init( okl, days );
  return loc;
}
