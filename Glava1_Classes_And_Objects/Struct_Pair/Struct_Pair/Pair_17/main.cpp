#include <iostream>

class Data17 {
  struct Pair {
    uint16_t first;
    uint16_t second;
  };
  static constexpr int WIDTH = 1600;
  static constexpr int HEIGHT = 1050;

 public:
  void Init( int16_t w, int16_t h );
  void Read( );
  void Display( ) const;
  void ChangeX( int16_t x );
  void ChangeY( int16_t y );

 private:
  Pair data_;
};

int main()
{
  Data17 dt;
  dt.Init( 800, 600 );
  dt.Display( );
  dt.ChangeX( 900 );
  dt.ChangeY( 700 );
  dt.Display( );
  dt.Read( );
  dt.Display( );
}

void Data17::Init( int16_t w, int16_t h ) {
  ChangeX( w );
  ChangeY( h );
}

void Data17::Read( ) {
  int16_t a, b;
  std::cout << "enter a, b" << std::endl;
  std::cin >> a >> b;
  this->Init( a, b );
}

void Data17::Display( ) const {
  std::cout << data_.first << "x" << data_.second << std::endl;
}

void Data17::ChangeX( int16_t x ) {
  if ( ( 0 < x && x <= WIDTH ) ) {
    data_.first = x;
  } else {
    std::cerr << "ERROR";
    exit( 1 );
  }
}

void Data17::ChangeY( int16_t y ) {
  if ( ( 0 < y && y <= HEIGHT ) ) {
    data_.second = y;
  } else {
    std::cerr << "ERROR";
    exit( 1 );
  }
}
