#include <iostream>

class Data13 {
  struct Pair {
    double first;
    double second;
  };

 public:
  void Init( double a = 0, double b = 0 );
  void Display( ) const;
  void Read( );
  double Hypotenuse( ) const;

 private:
  Pair data_;
};

Data13 make_Data13( double a, double b );

int main( ) {
  Data13 d;
  d.Init( 3, 4 );
  d.Display( );

  Data13 k;
  k.Read( );
  k.Display( );
}

void Data13::Init( double a, double b ) {
  if ( a < 0 || b < 0 ) {
    std::cout << "ERROR";
    exit( 1 );
  }
  data_.first = a;
  data_.second = b;
}

void Data13::Display( ) const {
  std::cout << data_.first << ":" << data_.second << " Hyp = " << Hypotenuse( )
            << std::endl;
}

void Data13::Read( ) {
  std::cout << "enter a, b" << std::endl;
  double a, b;
  std::cin >> a >> b;
  this->Init( a, b );
}

double Data13::Hypotenuse( ) const {
  return std::sqrt( std::pow( data_.first, 2 ) + std::pow( data_.second, 2 ) );
}

Data13 make_Data13( double a, double b ) {
  Data13 loc;
  loc.Init( a, b );
  return loc;
}
