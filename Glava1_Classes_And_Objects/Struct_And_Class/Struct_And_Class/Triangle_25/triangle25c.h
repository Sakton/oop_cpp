#ifndef TRIANGLE25C_H
#define TRIANGLE25C_H
#include <string>

// WARNING очень высокая связанность

struct DataTringle {
  enum class TYPE_TRIANGLE {
    RAVNO_STORONNY,
    RAVNO_BEDRENNY,
    PRYAMO_UGOLNY,
    OTHER
  };

  enum class TYPE_INITIALISATION { SSS = 0, SUS, USS, USU, UUS };

  double sA;
  double sB;
  double sC;
  double angA;
  double angB;
  double angC;

  void Init( double param1, double param2, double param3,
             TYPE_INITIALISATION type );
  void Display( ) const;
  // void Read( );
  std::string ToString( ) const;
};

class Triangle25C {
  friend struct DataTringle;

 public:
  void Init( double param1, double param2, double param3,
             DataTringle::TYPE_INITIALISATION type );

  void Display( ) const;
  // void Read( );
  std::string ToString( ) const;
  double GetA( ) const;
  double GetB( ) const;
  double GetC( ) const;
  double GetAngleA( ) const;
  double GetAngleB( ) const;
  double GetAngleC( ) const;

  void SetA( double size );
  void SetB( double size );
  void SetC( double size );
  void SetAngleA( double angle );
  void SetAngleB( double angle );
  void SetAngleC( double angle );

  double Area( ) const;
  double Perimeter( ) const;
  double HeightToA( ) const;
  double HeightToB( ) const;
  double HeightToC( ) const;
  DataTringle::TYPE_TRIANGLE GetType( ) const;

 private:
  //находим 3 сторону по 2 известным и углу
  static double TheoremaCos( double size1, double size2, double angle );
  //ищем угол противолежащий стороне x_size
  static double AnglePoTheoremeCos( double size1, double size2, double x_size );
  static double TheoremaSinAngle( double size1, double angle1, double x_size );
  static double ToRadian( double angle );
  static double ToAngle( double angle );
  static bool Posible( double a, double b, double c );

 private:
  DataTringle data_;
};

#endif // TRIANGLE25C_H
