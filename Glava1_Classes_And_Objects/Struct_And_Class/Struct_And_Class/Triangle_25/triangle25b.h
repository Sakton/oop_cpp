#ifndef TRIANGLE25B_H
#define TRIANGLE25B_H
#include <string>

class Triangle25B {
 public:
  enum class TYPE_TRIANGLE {
    RAVNO_STORONNY,
    RAVNO_BEDRENNY,
    PRYAMO_UGOLNY,
    OTHER
  };

  enum class TYPE_INITIALISATION { SSS = 0, SUS, USS, USU, UUS };

  // 2 строны и угол между ними, остальное решение треугольников
  void Init( double param1, double param2, double param3,
             TYPE_INITIALISATION type );

  void Display( ) const;
  // void Read( );
  std::string ToString( ) const;

  double GetA( ) const;
  double GetB( ) const;
  double GetC( ) const;
  double GetAngleA( ) const;
  double GetAngleB( ) const;
  double GetAngleC( ) const;

  void SetA( double size );
  void SetB( double size );
  void SetC( double size );
  void SetAngleA( double angle );
  void SetAngleB( double angle );
  void SetAngleC( double angle );

  double Area( ) const;
  double Perimeter( ) const;
  double HeightToA( ) const;
  double HeightToB( ) const;
  double HeightToC( ) const;

 private:
  //находим 3 сторону по 2 известным и углу
  double TheoremaCos( double size1, double size2, double angle );
  //ищем угол противолежащий стороне x_size
  double AnglePoTheoremeCos( double size1, double size2, double x_size );
  double TheoremaSinAngle( double size1, double angle1, double x_size );
  double ToRadian( double angle );
  double ToAngle( double angle );
  bool Posible( double a, double b, double c );

  TYPE_TRIANGLE GetType( ) const;

 private:
  double sA;
  double sB;
  double sC;
  double angA;
  double angB;
  double angC;
};

#endif // TRIANGLE25B_H
