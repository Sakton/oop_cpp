#include "triangle25a.h"
#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>

const double PI = 3.14159265358979323846;
const double PIx2 = PI * 2;
const int ANG_PI = 360;

double TheoremaCos( double size1, double size2, double angle ) {
  return std::sqrt( std::pow( size1, 2 ) + std::pow( size2, 2 ) -
                    2 * size1 * size2 * std::cos( ToRadian( angle ) ) );
}

double AnglePoTheoremeCos( double size1, double size2, double x_size ) {
  return ToAngle( std::acos(
      ( std::pow( size1, 2 ) + std::pow( size2, 2 ) - std::pow( x_size, 2 ) ) /
      2 / size1 / size2 ) );
}

double ToRadian( double angle ) { return angle * PIx2 / ANG_PI; }

double ToAngle( double angle ) { return angle * ANG_PI / PIx2; }

double TheoremaSinAngle( double size1, double angle1, double x_size ) {
  return ToAngle(
      std::asin( x_size * std::sin( ToRadian( angle1 ) ) / size1 ) );
}

bool Posible( double a, double b, double c ) {
  return ( ( a < b + c ) && ( b < a + c ) && ( c < a + b ) );
}

void Init( Triangle25A &tr, double param1, double param2, double param3,
           TYPE_INITIALISATION type ) {
  switch ( type ) {
    case TYPE_INITIALISATION::SUS: {
      tr.sA = param1;
      tr.sB = param3;
      tr.angC = param2;
      tr.sC = TheoremaCos( tr.sA, tr.sB, tr.angC );
      if ( !Posible( tr.sA, tr.sB, tr.sC ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      tr.angA = AnglePoTheoremeCos( tr.sB, tr.sC, tr.sA );
      tr.angB = AnglePoTheoremeCos( tr.sA, tr.sC, tr.sB );
    } break;
    case TYPE_INITIALISATION::SSS: {
      if ( !Posible( param1, param2, param3 ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      tr.sA = param1;
      tr.sB = param2;
      tr.sC = param3;
      tr.angA = AnglePoTheoremeCos( tr.sB, tr.sC, tr.sA );
      tr.angB = AnglePoTheoremeCos( tr.sA, tr.sC, tr.sB );
      tr.angC = AnglePoTheoremeCos( tr.sA, tr.sB, tr.sC );
    } break;
    case TYPE_INITIALISATION::USS: {
    } break;
    case TYPE_INITIALISATION::USU: {
    } break;
    case TYPE_INITIALISATION::UUS: {
    } break;
  }
}

void Display( const Triangle25A &tr ) {
  std::cout << ToString( tr ) << std::endl;
}

std::string ToString( const Triangle25A &tr ) {
  std::stringstream ss;
  ss << std::fixed << std::setprecision( 2 );
  ss << "storona a = " << tr.sA << std::endl
     << "storona b = " << tr.sB << std::endl
     << "storona c = " << tr.sC << std::endl
     << "ang alpha = " << tr.angA << std::endl
     << "ang betta = " << tr.angB << std::endl
     << "ang gamma = " << tr.angC << std::endl;
  return ss.str( );
}

double GetA( const Triangle25A &tr ) { return tr.sA; }

double GetB( const Triangle25A &tr ) { return tr.sB; }

double GetC( const Triangle25A &tr ) { return tr.sC; }

double GetAngleA( const Triangle25A &tr ) { return tr.sA; }

double GetAngleB( const Triangle25A &tr ) { return tr.angB; }

double GetAngleC( const Triangle25A &tr ) { return tr.angC; }

void SetA( Triangle25A &tr, double size ) {
  if ( !Posible( size, tr.sB, tr.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, size, tr.sB, tr.sC, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

void SetB( Triangle25A &tr, double size ) {
  if ( !Posible( tr.sA, size, tr.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, tr.sA, size, tr.sC, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

void SetC( Triangle25A &tr, double size ) {
  if ( !Posible( tr.sA, tr.sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, tr.sA, tr.sB, size, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

void SetAngleA( Triangle25A &tr, double angle ) {
  double size = TheoremaCos( tr.sB, tr.sC, angle );
  if ( !Posible( size, tr.sB, tr.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, size, tr.sB, tr.sC, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

void SetAngleB( Triangle25A &tr, double angle ) {
  double size = TheoremaCos( tr.sA, tr.sC, angle );
  if ( !Posible( tr.sA, size, tr.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, tr.sA, size, tr.sC, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

void SetAngleC( Triangle25A &tr, double angle ) {
  double size = TheoremaCos( tr.sA, tr.sB, angle );
  if ( !Posible( tr.sA, tr.sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25A tmp;
  Init( tmp, tr.sA, tr.sB, size, TYPE_INITIALISATION::SSS );
  tr = tmp;
}

double Area( const Triangle25A &tr ) {
  double p = Perimeter( tr ) / 2;
  return std::sqrt( p * ( p - tr.sA ) * ( p - tr.sB ) * ( p - tr.sC ) );
}

double Perimeter( const Triangle25A &tr ) { return tr.sA + tr.sB + tr.sC; }

double HeightToA( const Triangle25A &tr ) { return 2 * Area( tr ) / tr.sA; }

double HeightToB( const Triangle25A &tr ) { return 2 * Area( tr ) / tr.sB; }

double HeightToC( const Triangle25A &tr ) { return 2 * Area( tr ) / tr.sC; }

TYPE_TRIANGLE GetType( const Triangle25A &tr ) {
  if ( ( std::fabs( tr.sA - tr.sC ) < 1e-4 ) ||
       ( std::fabs( tr.sA - tr.sB ) < 1e-4 ) ||
       ( std::fabs( tr.sB - tr.sC ) < 1e-4 ) ) {
    return TYPE_TRIANGLE::RAVNO_BEDRENNY;
  }
  if ( ( std::fabs( tr.angA - 90.000 ) < 1e-4 ) ||
       ( std::fabs( tr.angB - 90.000 ) < 1e-4 ) ||
       ( std::fabs( tr.angC - 90.000 ) < 1e-4 ) ) {
    return TYPE_TRIANGLE::PRYAMO_UGOLNY;
  }
  if ( ( ( std::fabs( tr.sA - tr.sC ) < 1e-4 ) ||
         ( std::fabs( tr.sA - tr.sB ) < 1e-4 ) ||
         ( std::fabs( tr.sB - tr.sC ) < 1e-4 ) ) &&
       ( ( std::fabs( tr.angA - tr.angC ) < 1e-4 ) ||
         ( std::fabs( tr.angA - tr.angB ) < 1e-4 ) ||
         ( std::fabs( tr.angB - tr.angC ) < 1e-4 ) ) ) {
    return TYPE_TRIANGLE::RAVNO_STORONNY;
  }
  return TYPE_TRIANGLE::OTHER;
}
