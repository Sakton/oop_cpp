#include "triangle25b.h"
#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>

const double PI = 3.14159265358979323846;
const double PIx2 = PI * 2;
const int ANG_PI = 360;

double Triangle25B::TheoremaCos( double size1, double size2, double angle ) {
  return std::sqrt( std::pow( size1, 2 ) + std::pow( size2, 2 ) -
                    2 * size1 * size2 * std::cos( ToRadian( angle ) ) );
}

double Triangle25B::AnglePoTheoremeCos( double size1, double size2,
                                        double x_size ) {
  return ToAngle( std::acos(
      ( std::pow( size1, 2 ) + std::pow( size2, 2 ) - std::pow( x_size, 2 ) ) /
      2 / size1 / size2 ) );
}

double Triangle25B::ToRadian( double angle ) { return angle * PIx2 / ANG_PI; }

double Triangle25B::ToAngle( double angle ) { return angle * ANG_PI / PIx2; }

double Triangle25B::TheoremaSinAngle( double size1, double angle1,
                                      double x_size ) {
  return ToAngle(
      std::asin( x_size * std::sin( ToRadian( angle1 ) ) / size1 ) );
}

bool Triangle25B::Posible( double a, double b, double c ) {
  return ( ( a < b + c ) && ( b < a + c ) && ( c < a + b ) );
}

void Triangle25B::Init( double param1, double param2, double param3,
                        Triangle25B::TYPE_INITIALISATION type ) {
  switch ( type ) {
    case Triangle25B::TYPE_INITIALISATION::SUS: {
      sA = param1;
      sB = param3;
      angC = param2;
      sC = TheoremaCos( sA, sB, angC );
      if ( !Posible( sA, sB, sC ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      angA = AnglePoTheoremeCos( sB, sC, sA );
      angB = AnglePoTheoremeCos( sA, sC, sB );
    } break;
    case Triangle25B::TYPE_INITIALISATION::SSS: {
      if ( !Posible( param1, param2, param3 ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      sA = param1;
      sB = param2;
      sC = param3;
      angA = AnglePoTheoremeCos( sB, sC, sA );
      angB = AnglePoTheoremeCos( sA, sC, sB );
      angC = AnglePoTheoremeCos( sA, sB, sC );
    } break;
    case Triangle25B::TYPE_INITIALISATION::USS: {
    } break;
    case Triangle25B::TYPE_INITIALISATION::USU: {
    } break;
    case Triangle25B::TYPE_INITIALISATION::UUS: {
    } break;
  }
}

void Triangle25B::Display( ) const { std::cout << ToString( ) << std::endl; }

std::string Triangle25B::ToString( ) const {
  std::stringstream ss;
  ss << std::fixed << std::setprecision( 2 );
  ss << "storona a = " << sA << std::endl
     << "storona b = " << sB << std::endl
     << "storona c = " << sC << std::endl
     << "ang alpha = " << angA << std::endl
     << "ang betta = " << angB << std::endl
     << "ang gamma = " << angC << std::endl;
  return ss.str( );
}

double Triangle25B::GetA( ) const { return sA; }

double Triangle25B::GetB( ) const { return sB; }

double Triangle25B::GetC( ) const { return sC; }

double Triangle25B::GetAngleA( ) const { return sA; }

double Triangle25B::GetAngleB( ) const { return angB; }

double Triangle25B::GetAngleC( ) const { return angC; }

void Triangle25B::SetA( double size ) {
  if ( !Posible( size, sB, sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( size, sB, sC, Triangle25B::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25B::SetB( double size ) {
  if ( !Posible( sA, size, sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( sA, size, sC, TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25B::SetC( double size ) {
  if ( !Posible( sA, sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( sA, sB, size, TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25B::SetAngleA( double angle ) {
  double size = TheoremaCos( sB, sC, angle );
  if ( !Posible( size, sB, sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( size, sB, sC, TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25B::SetAngleB( double angle ) {
  double size = TheoremaCos( sA, sC, angle );
  if ( !Posible( sA, size, sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( sA, size, sC, TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25B::SetAngleC( double angle ) {
  double size = TheoremaCos( sA, sB, angle );
  if ( !Posible( sA, sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25B tmp;
  tmp.Init( sA, sB, size, TYPE_INITIALISATION::SSS );
  *this = tmp;
}

double Triangle25B::Area( ) const {
  double p = Perimeter( ) / 2;
  return std::sqrt( p * ( p - sA ) * ( p - sB ) * ( p - sC ) );
}

double Triangle25B::Perimeter( ) const { return sA + sB + sC; }

double Triangle25B::HeightToA( ) const { return 2 * Area( ) / sA; }

double Triangle25B::HeightToB( ) const { return 2 * Area( ) / sB; }

double Triangle25B::HeightToC( ) const { return 2 * Area( ) / sC; }

Triangle25B::TYPE_TRIANGLE Triangle25B::GetType( ) const {
  if ( ( std::fabs( sA - sC ) < 1e-4 ) || ( std::fabs( sA - sB ) < 1e-4 ) ||
       ( std::fabs( sB - sC ) < 1e-4 ) ) {
    return TYPE_TRIANGLE::RAVNO_BEDRENNY;
  }
  if ( ( std::fabs( angA - 90.000 ) < 1e-4 ) ||
       ( std::fabs( angB - 90.000 ) < 1e-4 ) ||
       ( std::fabs( angC - 90.000 ) < 1e-4 ) ) {
    return TYPE_TRIANGLE::PRYAMO_UGOLNY;
  }
  if ( ( ( std::fabs( sA - sC ) < 1e-4 ) || ( std::fabs( sA - sB ) < 1e-4 ) ||
         ( std::fabs( sB - sC ) < 1e-4 ) ) &&
       ( ( std::fabs( angA - angC ) < 1e-4 ) ||
         ( std::fabs( angA - angB ) < 1e-4 ) ||
         ( std::fabs( angB - angC ) < 1e-4 ) ) ) {
    return TYPE_TRIANGLE::RAVNO_STORONNY;
  }
  return TYPE_TRIANGLE::OTHER;
}
