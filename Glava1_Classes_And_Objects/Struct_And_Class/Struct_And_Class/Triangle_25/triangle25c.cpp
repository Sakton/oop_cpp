#include "triangle25c.h"
#include <cmath>
#include <iomanip>
#include <iostream>
#include <sstream>

const double PI = 3.14159265358979323846;
const double PIx2 = PI * 2;
const int ANG_PI = 360;

void DataTringle::Init( double param1, double param2, double param3,
                        DataTringle::TYPE_INITIALISATION type ) {
  switch ( type ) {
    case DataTringle::TYPE_INITIALISATION::SUS: {
      sA = param1;
      sB = param3;
      angC = param2;
      sC = Triangle25C::TheoremaCos( sA, sB, angC );
      if ( !Triangle25C::Posible( sA, sB, sC ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      angA = Triangle25C::AnglePoTheoremeCos( sB, sC, sA );
      angB = Triangle25C::AnglePoTheoremeCos( sA, sC, sB );
    } break;
    case DataTringle::TYPE_INITIALISATION::SSS: {
      if ( !Triangle25C::Posible( param1, param2, param3 ) ) {
        std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
        exit( 1 );
      }
      sA = param1;
      sB = param2;
      sC = param3;
      angA = Triangle25C::AnglePoTheoremeCos( sB, sC, sA );
      angB = Triangle25C::AnglePoTheoremeCos( sA, sC, sB );
      angC = Triangle25C::AnglePoTheoremeCos( sA, sB, sC );
    } break;
    case DataTringle::TYPE_INITIALISATION::USS: {
    } break;
    case DataTringle::TYPE_INITIALISATION::USU: {
    } break;
    case DataTringle::TYPE_INITIALISATION::UUS: {
    } break;
  }
}

void DataTringle::Display( ) const { std::cout << ToString( ) << std::endl; }

std::string DataTringle::ToString( ) const {
  std::stringstream ss;
  ss << std::fixed << std::setprecision( 2 );
  ss << "storona a = " << sA << std::endl
     << "storona b = " << sB << std::endl
     << "storona c = " << sC << std::endl
     << "ang alpha = " << angA << std::endl
     << "ang betta = " << angB << std::endl
     << "ang gamma = " << angC << std::endl;
  return ss.str( );
}

//*****************

double Triangle25C::TheoremaCos( double size1, double size2, double angle ) {
  return std::sqrt( std::pow( size1, 2 ) + std::pow( size2, 2 ) -
                    2 * size1 * size2 * std::cos( ToRadian( angle ) ) );
}

double Triangle25C::AnglePoTheoremeCos( double size1, double size2,
                                        double x_size ) {
  return ToAngle( std::acos(
      ( std::pow( size1, 2 ) + std::pow( size2, 2 ) - std::pow( x_size, 2 ) ) /
      2 / size1 / size2 ) );
}

double Triangle25C::ToRadian( double angle ) { return angle * PIx2 / ANG_PI; }

double Triangle25C::ToAngle( double angle ) { return angle * ANG_PI / PIx2; }

double Triangle25C::TheoremaSinAngle( double size1, double angle1,
                                      double x_size ) {
  return ToAngle(
      std::asin( x_size * std::sin( ToRadian( angle1 ) ) / size1 ) );
}

bool Triangle25C::Posible( double a, double b, double c ) {
  return ( ( a < b + c ) && ( b < a + c ) && ( c < a + b ) );
}

void Triangle25C::Init( double param1, double param2, double param3,
                        DataTringle::TYPE_INITIALISATION type ) {
  data_.Init( param1, param2, param3, type );
}

void Triangle25C::Display( ) const { data_.Display( ); }

std::string Triangle25C::ToString( ) const { return data_.ToString( ); }

double Triangle25C::GetA( ) const { return data_.sA; }

double Triangle25C::GetB( ) const { return data_.sB; }

double Triangle25C::GetC( ) const { return data_.sC; }

double Triangle25C::GetAngleA( ) const { return data_.sA; }

double Triangle25C::GetAngleB( ) const { return data_.angB; }

double Triangle25C::GetAngleC( ) const { return data_.angC; }

void Triangle25C::SetA( double size ) {
  if ( !Posible( size, data_.sB, data_.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( size, data_.sB, data_.sC, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25C::SetB( double size ) {
  if ( !Posible( data_.sA, size, data_.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( data_.sA, size, data_.sC, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25C::SetC( double size ) {
  if ( !Posible( data_.sA, data_.sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( data_.sA, data_.sB, size, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25C::SetAngleA( double angle ) {
  double size = TheoremaCos( data_.sB, data_.sC, angle );
  if ( !Posible( size, data_.sB, data_.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( size, data_.sB, data_.sC, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25C::SetAngleB( double angle ) {
  double size = TheoremaCos( data_.sA, data_.sC, angle );
  if ( !Posible( data_.sA, size, data_.sC ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( data_.sA, size, data_.sC, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

void Triangle25C::SetAngleC( double angle ) {
  double size = TheoremaCos( data_.sA, data_.sB, angle );
  if ( !Posible( data_.sA, data_.sB, size ) ) {
    std::cerr << "ERROR TRIANGLE IMPOSSIBLE";
    exit( 1 );
  }
  Triangle25C tmp;
  tmp.Init( data_.sA, data_.sB, size, DataTringle::TYPE_INITIALISATION::SSS );
  *this = tmp;
}

double Triangle25C::Area( ) const {
  double p = Perimeter( ) / 2;
  return std::sqrt( p * ( p - data_.sA ) * ( p - data_.sB ) *
                    ( p - data_.sC ) );
}

double Triangle25C::Perimeter( ) const {
  return data_.sA + data_.sB + data_.sC;
}

double Triangle25C::HeightToA( ) const { return 2 * Area( ) / data_.sA; }

double Triangle25C::HeightToB( ) const { return 2 * Area( ) / data_.sB; }

double Triangle25C::HeightToC( ) const { return 2 * Area( ) / data_.sC; }

DataTringle::TYPE_TRIANGLE Triangle25C::GetType( ) const {
  if ( ( std::fabs( data_.sA - data_.sC ) < 1e-4 ) ||
       ( std::fabs( data_.sA - data_.sB ) < 1e-4 ) ||
       ( std::fabs( data_.sB - data_.sC ) < 1e-4 ) ) {
    return DataTringle::TYPE_TRIANGLE::RAVNO_BEDRENNY;
  }
  if ( ( std::fabs( data_.angA - 90.000 ) < 1e-4 ) ||
       ( std::fabs( data_.angB - 90.000 ) < 1e-4 ) ||
       ( std::fabs( data_.angC - 90.000 ) < 1e-4 ) ) {
    return DataTringle::TYPE_TRIANGLE::PRYAMO_UGOLNY;
  }
  if ( ( ( std::fabs( data_.sA - data_.sC ) < 1e-4 ) ||
         ( std::fabs( data_.sA - data_.sB ) < 1e-4 ) ||
         ( std::fabs( data_.sB - data_.sC ) < 1e-4 ) ) &&
       ( ( std::fabs( data_.angA - data_.angC ) < 1e-4 ) ||
         ( std::fabs( data_.angA - data_.angB ) < 1e-4 ) ||
         ( std::fabs( data_.angB - data_.angC ) < 1e-4 ) ) ) {
    return DataTringle::TYPE_TRIANGLE::RAVNO_STORONNY;
  }
  return DataTringle::TYPE_TRIANGLE::OTHER;
}
