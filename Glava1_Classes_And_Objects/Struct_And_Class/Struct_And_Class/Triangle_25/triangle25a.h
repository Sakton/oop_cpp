#ifndef TRIANGLE25A_H
#define TRIANGLE25A_H
#include <string>

/*
http://poivs.tsput.ru/ru/Math/Functions/ElementaryFunctions/Trigonometry/SolvingTriangles
Для нахождения неизвестного угла надёжнее использовать теорему косинусов, а не
синусов. Причина в том, что значение синуса угла при вершине треугольника не
определяет однозначно самого угла. Например, если sinβ=0,5, то угол β может быть
как 30∘, так и 150∘, потому что синусы этих углов совпадают. Исключением
является случай, когда заранее известно, что в данном треугольнике тупых углов
быть не может — например, если треугольник прямоугольный. С косинусом такие
проблемы не возникают, в интервале от 0∘ до 180∘ значение косинуса определяет
угол однозначно.

Три стороны (ССС);  
Две стороны и угол между ними (СУС);  
Две стороны и угол напротив одной из них (УСС);  
Сторона и два прилежащих угла (УСУ);  
Сторона, противолежащий угол и один из прилежащих (УУС)
*/

struct Triangle25A {
  double sA;
  double sB;
  double sC;
  double angA;
  double angB;
  double angC;
};

enum class TYPE_TRIANGLE {
  RAVNO_STORONNY,
  RAVNO_BEDRENNY,
  PRYAMO_UGOLNY,
  OTHER
};

enum class TYPE_INITIALISATION { SSS = 0, SUS, USS, USU, UUS };

// 2 строны и угол между ними, остальное решение треугольников
void Init( Triangle25A& tr, double param1, double param2, double param3,
           TYPE_INITIALISATION type );

void Display( const Triangle25A& tr );
void Read( Triangle25A& tr );
std::string ToString( const Triangle25A& tr );

double GetA( const Triangle25A& tr );
double GetB( const Triangle25A& tr );
double GetC( const Triangle25A& tr );
double GetAngleA( const Triangle25A& tr );
double GetAngleB( const Triangle25A& tr );
double GetAngleC( const Triangle25A& tr );

void SetA( Triangle25A& tr, double size );
void SetB( Triangle25A& tr, double size );
void SetC( Triangle25A& tr, double size );
void SetAngleA( Triangle25A& tr, double angle );
void SetAngleB( Triangle25A& tr, double angle );
void SetAngleC( Triangle25A& tr, double angle );

double Area( const Triangle25A& tr );
double Perimeter( const Triangle25A& tr );
double HeightToA( const Triangle25A& tr );
double HeightToB( const Triangle25A& tr );
double HeightToC( const Triangle25A& tr );

//находим 3 сторону по 2 известным и углу
double TheoremaCos( double size1, double size2, double angle );
//ищем угол противолежащий стороне x_size
double AnglePoTheoremeCos( double size1, double size2, double x_size );
double TheoremaSinAngle( double size1, double angle1, double x_size );
double ToRadian( double angle );
double ToAngle( double angle );
bool Posible( double a, double b, double c );

TYPE_TRIANGLE GetType( const Triangle25A& tr );

#endif  // TRIANGLE25A_H
