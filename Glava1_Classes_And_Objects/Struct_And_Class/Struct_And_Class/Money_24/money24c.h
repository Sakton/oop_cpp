#ifndef MONEY24C_H
#define MONEY24C_H
#include <string>

struct DataMoney {
  int32_t rub { 0 };
  uint8_t cop { 0 };

  void Init( int32_t rb, int16_t cp );
  void Display( ) const;
  void Read( );
  std::string ToString( ) const;
};

class Money24C {
 public:
  void Init( int32_t rb, int16_t cp );
  void Display( ) const;
  void Read( );
  std::string ToString( ) const;

  Money24C Add( const Money24C& b ) const;
  Money24C Sub( const Money24C& b ) const;
  Money24C Mul( double x ) const;
  double Div( const Money24C& b ) const;
  Money24C Div( double x ) const;
  bool Eq( const Money24C& b ) const;
  bool NotEqu( const Money24C& b ) const;
  bool Less( const Money24C& b ) const;
  bool LessOrEq( const Money24C& b ) const;
  bool Greater( const Money24C& b ) const;
  bool GreaterOrEq( const Money24C& b ) const;

 private:
  DataMoney data;
};

#endif  // MONEY24C_H
