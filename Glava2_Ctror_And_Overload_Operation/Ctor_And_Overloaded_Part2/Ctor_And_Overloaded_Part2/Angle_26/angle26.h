#ifndef ANGLE26_H
#define ANGLE26_H
#include <string>

class Angle26 {
 public:
  void Init( int16_t g, int16_t m );
  void Init( double angl );
  void Read( );
  void Display( ) const;
  std::string ToString( ) const;

  double ToRadian( ) const;
  Angle26 Normalise( ) const;
  void Normalise( );
  Angle26 Adding( int16_t g, int16_t m ) const;
  Angle26 Adding( const Angle26& other ) const;
  Angle26 Substr( int16_t g, int16_t m ) const;
  Angle26 Substr( const Angle26& other ) const;
  double Sinus( ) const;
  double Angle( ) const;

  bool Eq( const Angle26& an2 );
  bool NotEq( const Angle26& an2 );
  bool Less( const Angle26& an2 );
  bool LessEq( const Angle26& an2 );
  bool Greater( const Angle26& an2 );
  bool GreaterEq( const Angle26& an2 );

 private:
  int16_t grad;
  int16_t min;
};

#endif // ANGLE26_H
