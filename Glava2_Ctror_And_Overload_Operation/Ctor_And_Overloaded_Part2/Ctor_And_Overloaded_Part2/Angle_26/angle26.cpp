#include "angle26.h"

#include <iostream>
#include <sstream>

const double PI = 3.14159265358979323846;
const int ANG_PI_2 = 180;

void Angle26::Init( int16_t g, int16_t m ) {
  //знаки должны быть одинаковы раз передается 2 параметрами
  if ( ( g < 0 && m > 0 ) || ( 0 < g && m < 0 ) ||
       !( 0 <= std::abs( m ) && std::abs( m ) < 60 ) ) {
    std::cout << "ERROR DATA";
    exit( 1 );
  }
  grad = g;
  min = m;
}

void Angle26::Init( double angl ) {
  grad = static_cast< int16_t >( angl );
  min = static_cast< int16_t >( ( angl - grad ) * 60 );
}

void Angle26::Read( ) {
  int16_t a, b;
  std::cout << "Enter a, b" << std::endl;
  std::cin >> a >> b;
  Init( a, b );
}

void Angle26::Display( ) const { std::cout << ToString( ) << std::endl; }

std::string Angle26::ToString( ) const {
  std::stringstream ss;
  ss << grad << static_cast< char >( 248 ) << std::abs( min ) << "\'";
  return ss.str( );
}

double Angle26::ToRadian( ) const {
  double grad = static_cast< double >( this->grad ) +
                static_cast< double >( min ) * 60 / 100;
  return grad * PI / ANG_PI_2;
}

Angle26 Angle26::Normalise( ) const {
  Angle26 t = *this;
  t.grad %= 360;
  return t;
}

void Angle26::Normalise( ) { grad %= 360; }

Angle26 Angle26::Adding( int16_t g, int16_t m ) const {
  Angle26 t = *this;
  t.grad += g;
  t.min += m;
  if ( std::abs( t.min ) >= 60 ) {
    t.grad = ( t.grad < 0 ) ? --t.grad : ++t.grad;
    t.min = ( t.min < 0 ) ? t.min + 60 : t.min - 60;
  }
  return t;
}

Angle26 Angle26::Adding( const Angle26& other ) const {
  Angle26 t = *this;
  t.grad += other.grad;
  t.min += other.min;
  if ( std::abs( t.min ) >= 60 ) {
    t.grad = ( t.grad < 0 ) ? --t.grad : ++t.grad;
    t.min = ( t.min < 0 ) ? t.min + 60 : t.min - 60;
  }
  return t;
}

Angle26 Angle26::Substr( int16_t g, int16_t m ) const {
  Angle26 t = *this;
  t.grad -= g;
  t.min -= m;
  if ( std::abs( t.min ) >= 60 ) {
    t.grad = ( t.grad < 0 ) ? --t.grad : ++t.grad;
    t.min = ( t.min < 0 ) ? t.min + 60 : t.min - 60;
  }
  return t;
}

Angle26 Angle26::Substr( const Angle26& other ) const {
  Angle26 t = *this;
  t.grad -= other.grad;
  t.min -= other.min;
  if ( std::abs( t.min ) >= 60 ) {
    t.grad = ( t.grad < 0 ) ? --t.grad : ++t.grad;
    t.min = ( t.min < 0 ) ? t.min + 60 : t.min - 60;
  }
  return t;
}

double Angle26::Sinus( ) const { return std::sin( ToRadian( ) ); }

double Angle26::Angle( ) const {
  return ( double( grad ) + double( min ) / 60 );
}

bool Angle26::Eq( const Angle26& an2 ) {
  return ( grad == an2.grad ) && ( min == an2.min );
}

bool Angle26::NotEq( const Angle26& an2 ) { return !Eq( an2 ); }

bool Angle26::Less( const Angle26& an2 ) {
  return ( grad < an2.grad ) || ( ( grad == an2.grad ) && ( min < an2.min ) );
}

bool Angle26::LessEq( const Angle26& an2 ) { return Less( an2 ) || Eq( an2 ); }

bool Angle26::Greater( const Angle26& an2 ) { return !LessEq( an2 ); }

bool Angle26::GreaterEq( const Angle26& an2 ) {
  return Greater( an2 ) || Eq( an2 );
}
